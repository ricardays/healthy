FROM openjdk:11-buster

RUN mkdir /usr/local/healthy -p
WORKDIR /usr/local/healthy

COPY target/healthy-0.0.1-SNAPSHOT.jar /usr/local/healthy/healthy-0.0.1-SNAPSHOT.jar

#HEALTHCHECK --interval=10s --timeout=10s --start-period=30s --retries=3 CMD curl --fail http://localhost:8080/health || kill 1

ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","healthy-0.0.1-SNAPSHOT.jar"]