package com.jacidi.healthy;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.LocalDate;
import java.util.HashMap;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/")
@Slf4j
public class MainController {

    Integer count = 0;

    @CrossOrigin(origins = "*")
    @GetMapping(path = "/health")
    public ResponseEntity<HashMap<String, String>> health() throws Exception {
        HashMap<String, String> map = new HashMap<>();
        map.put("value","OK");
        if (count< 5){
            log.info("Health checked: " + LocalDate.now());
            count += 1;
            log.info("Count: " + count);
            return new ResponseEntity<>(map, HttpStatus.OK);
        }else {
            count += 1;
            throw new Exception("Managed Exception, count: " + count);
        }

    }
}
